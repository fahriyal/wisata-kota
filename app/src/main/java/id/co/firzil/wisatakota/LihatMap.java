package id.co.firzil.wisatakota;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Fahriyal Afif on 6/1/2016.
 */
public class LihatMap extends AppCompatActivity implements OnMapReadyCallback, LocationListener{

    private LocationManager locationManager;
    private LatLng current;
    private AlertDialog alert;
    private ArrayList<Polyline> rute;

    protected void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.lihat_map);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Location now = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(now != null){
            current = new LatLng(now.getLatitude(), now.getLongitude());
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    protected void onResume(){
        super.onResume();
        cekGps();
    }

    private void cekGps(){
        //jika gps sudah aktif
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 6 * 1000, 0, this);
        }
        else{
            if(alert == null){
                alert = new AlertDialog.Builder(this)
                        .setTitle("Pengaturan GPS")
                        .setMessage("GPS Mati, nyalakan GPS ?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .create();
            }
            if(! alert.isShowing()) alert.show();
        }
    }

    protected void onDestroy(){
        super.onDestroy();
        locationManager.removeUpdates(this);
    }

    private HashMap<Marker, Place> marker_data = new HashMap<>();
    private GoogleMap gmap;

    private void addMarker(double lat, double lon, int gambar, String nama, String desk){
        Marker marker = gmap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, lon))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        Place p = new Place(gambar, nama, desk);
        marker_data.put(marker, p);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        gmap = map;
        gmap.getUiSettings().setMyLocationButtonEnabled(true);
        gmap.setMyLocationEnabled(true);


        LatLng lt = new LatLng(-7.2674604,112.7400695);
        gmap.moveCamera(CameraUpdateFactory.newLatLngZoom(lt, 10));

        addMarker(-7.2702833, 112.7248424, R.drawable.tmii, "TMII", "Taman Mini Indonesia Indah cuyy");
        addMarker(-7.2674604, 112.7400695, R.drawable.hi, "Bundaran HI", "Bundaran Hotel Indonesia terletak di pusat kota Jakarta");
        addMarker(-7.288854, 112.801834, R.drawable.monas, "Monas", "Monumen Nasional Jakarta");

        gmap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker m) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker marker) {
                Place p = marker_data.get(marker);

                View v = getLayoutInflater().inflate(R.layout.popup_marker, null);
                TextView nama = (TextView) v.findViewById(R.id.nama_tempat),
                        deskripsi = (TextView) v.findViewById(R.id.deskripsi);
                ImageView gambar = (ImageView) v.findViewById(R.id.gambar);

                nama.setText(p.nama);
                deskripsi.setText(p.deskripsi);
                gambar.setImageResource(p.gambar);

                return v;
            }
        });

        gmap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                LatLng dest = marker.getPosition();
                if(current != null){
                    final String MY_SERVER_KEY = "AIzaSyAg8w6vjUxSi_WoxfONDiioi3wnnFgHwmA";  //ganti dengan server key mu

                    String url = "https://maps.googleapis.com/maps/api/directions/json?"
                            +"origin="+current.latitude+","+current.longitude
                            +"&destination="+dest.latitude+","+dest.longitude
                            +"&key="+MY_SERVER_KEY;


                    new AsyncTask<String, String, String>(){
                        ProgressDialog pd;

                        protected void onPreExecute(){
                            super.onPreExecute();
                            pd = new ProgressDialog(LihatMap.this);
                            pd.setMessage("Getting route, please wait..");
                            pd.setIndeterminate(true);
                            pd.setCanceledOnTouchOutside(false);
                            pd.setCancelable(true);
                            pd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialog) {
                                    cancel(true);
                                }
                            });
                            pd.show();
                        }

                        @Override
                        protected String doInBackground(String... params) {
                            String data = "";
                            try{
                                //menjalankan url yg dipanggil
                                URL url = new URL(params[0]);

                                // membuat http connection untuk komunikasi dengan url
                                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                                // koneksi dengan url
                                urlConnection.connect();

                                // Membaca data dari url
                                InputStream iStream = urlConnection.getInputStream();

                                BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

                                StringBuilder sb  = new StringBuilder();

                                String line = "";
                                while( ( line = br.readLine())  != null){
                                    sb.append(line);
                                }
                                data = sb.toString();
                                br.close();

                                iStream.close();
                                urlConnection.disconnect();

                            }catch(Exception e){
                                Log.d("Exception", e.toString());
                            }
                            return data;
                        }

                        protected void onPostExecute(String result){
                            super.onPostExecute(result);
                            pd.dismiss();

                            try {
                                JSONObject js = new JSONObject(result);
                                JSONArray routes = js.optJSONArray("routes");
                                if(routes != null && routes.length() > 0) {
                                    if(rute == null) rute = new ArrayList<>();
                                    else if(rute.size() > 0){   //dihapus dulu semua rute yg muncul di gmaps
                                        for(int i=0; i<rute.size(); i++) {
                                            Polyline per_rute = rute.get(i);
                                            per_rute.remove();
                                        }
                                        rute.clear();
                                    }

                                    for (int i = 0; i < routes.length(); i++) {
                                        List<LatLng> list_ltln = new ArrayList<>();
                                        JSONObject ob = routes.getJSONObject(i);
                                        JSONArray legs = ob.optJSONArray("legs");
                                        for (int j = 0; j < legs.length(); j++) {
                                            JSONObject per_leg = legs.getJSONObject(j);
                                            JSONArray steps = per_leg.optJSONArray("steps");
                                            for (int k = 0; k < steps.length(); k++) {
                                                String encoded_point = steps.getJSONObject(k).getJSONObject("polyline").getString("points");
                                                list_ltln.addAll(PolyUtil.decode(encoded_point));
                                            }
                                        }

                                        Random rnd = new Random(); //generate warna random
                                        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                                        rute.add(gmap.addPolyline(new PolylineOptions().color(color).addAll(list_ltln)));
                                    }
                                }

                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(LihatMap.this, "Error menampilkan rute", Toast.LENGTH_LONG).show();
                            }
                        }

                    }.execute(url);
                }
                else {
                    cekGps();
                    Toast.makeText(LihatMap.this, "Sedang mencari lokasi gps...", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onLocationChanged(Location l) {
        current = new LatLng(l.getLatitude(), l.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private class Place{
        public int gambar;
        public String nama, deskripsi;
        public Place(int gambar, String nama, String deskripsi){
            this.nama = nama;
            this.gambar = gambar;
            this.deskripsi = deskripsi;
        }
    }

}
